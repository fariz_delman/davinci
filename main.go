package main

import (
	"log"

	"gitlab.com/fariz_delman/davinci/db"
	"gitlab.com/fariz_delman/davinci/http/server"
)

func main() {
	withDB, err := db.New()

	if err != nil {
		log.Fatalf("failed to connect to redis: %s", err.Error())
	}

	err = server.Init(withDB)

	if err != nil {
		log.Fatalf("failed to run server: %s", err.Error())
	}
}

package server

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/django"

	"gitlab.com/fariz_delman/davinci/db"
	"gitlab.com/fariz_delman/davinci/http/controllers"

	"gitlab.com/fariz_delman/davinci/utils/helpers"
	"gitlab.com/fariz_delman/davinci/utils/template"
)

// Init is
func Init(db *db.Database) error {
	controller := controllers.New(db)
	engine := django.New(template.BaseTemplateDir, template.TemplateSuffix)
	app := fiber.New(fiber.Config{
		Views: engine,
	})

	if helpers.IsDevelopment() {
		engine.Reload(true)
		engine.Debug(true)
	}

	app.Get("/__healthcheck", func(c *fiber.Ctx) error {
		return c.SendString("OK")
	})

	// life is too short
	app.Get("/favicon.ico", func(c *fiber.Ctx) error {
		return c.SendStatus(404)
	})

	app.Use(logger.New())

	app.Static("/static", template.StaticDir)
	app.Static("/videos", template.VideoDir)

	app.Get("/", controller.ListProjects)
	app.Get("/:ProjectID", controller.ProjectDetail)
	app.Get("/:ProjectID/:Version", controller.ProjectVersion)

	err := app.Listen(":1337")

	if err != nil {
		return err
	}

	return nil
}

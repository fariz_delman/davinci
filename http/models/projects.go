package models

import (
	"context"

	"gitlab.com/fariz_delman/davinci/db"
	"gitlab.com/fariz_delman/davinci/utils/key"
)

// GetAllProjects is
func GetAllProjects(ctx context.Context, db *db.Database) ([]string, error) {
	projects, err := db.Redis.LRange(ctx, key.Projects, 0, -1).Result()

	if err != nil {
		return nil, err
	}

	return projects, nil
}

// GetGitlabProjectID is
func GetGitlabProjectID(ctx context.Context, db *db.Database, projectID string) (string, error) {
	gitlabProjectID, err := db.Redis.HGet(ctx, key.Project+projectID, key.GitlabProjectID).Result()

	if err != nil {
		return "", err
	}

	return gitlabProjectID, nil
}

// GetProjectDetail is
func GetProjectDetail(ctx context.Context, db *db.Database, projectID string) (map[string]string, error) {
	project, err := db.Redis.HGetAll(ctx, key.Project+projectID).Result()

	if err != nil {
		return nil, err
	}

	return project, err
}

// GetVersionName is
func GetVersionName(ctx context.Context, db *db.Database, versionID string) (string, error) {
	versionName, err := db.Redis.Get(ctx, key.Version+versionID+":name").Result()

	if err != nil {
		return "", err
	}

	return versionName, err
}

// GetProjectName is
func GetProjectName(ctx context.Context, db *db.Database, projectID string) (string, error) {
	projectName, err := db.Redis.HGet(ctx, key.Project+projectID, "name").Result()

	if err != nil {
		return "", err
	}

	return projectName, err
}

// GetVersions is
func GetVersions(ctx context.Context, db *db.Database, projectID string) ([]string, error) {
	versions, err := db.Redis.LRange(ctx, key.Versions+projectID, 0, -1).Result()

	if err != nil {
		return nil, err
	}

	return versions, nil
}

// GetBackends is
func GetBackends(ctx context.Context, db *db.Database, projectID string) ([]string, error) {
	backends, err := db.Redis.LRange(ctx, key.Backends+projectID, 0, -1).Result()

	if err != nil {
		return nil, err
	}

	return backends, nil
}

// GetFrontend is
func GetFrontend(ctx context.Context, db *db.Database, projectID string) (string, error) {
	frontend, err := db.Redis.Get(ctx, key.Frontend+projectID).Result()

	if err != nil {
		return "", err
	}

	return frontend, nil
}

// GetScenarios is
func GetScenarios(ctx context.Context, db *db.Database, versionID string) ([]string, error) {
	scenarios, err := db.Redis.LRange(ctx, key.Scenarios+versionID, 0, -1).Result()

	if err != nil {
		return nil, err
	}

	return scenarios, err
}

// SetScenarios is
func SetScenarios(ctx context.Context, db *db.Database, versionID string, datas []string) {
	for _, data := range datas {
		db.Redis.LPush(ctx, key.Scenarios+versionID, data)
	}
}

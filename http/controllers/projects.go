package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/fariz_delman/davinci/http/models"
	"gitlab.com/fariz_delman/davinci/utils/helpers"
	"gitlab.com/fariz_delman/davinci/utils/key"
	"gitlab.com/fariz_delman/davinci/utils/template"

	gitlab "gitlab.com/fariz_delman/davinci/http/services"
)

// IDAndName is
type IDAndName struct {
	ID   string
	Name string
}

// Container is
type Container struct {
	ID        string
	ImageName string
	State     string
}

// ProjectVersion is
func (r *Router) ProjectVersion(c *fiber.Ctx) error {
	var runningContainers []Container

	projectID := c.Params("ProjectID")
	version := c.Params("Version")

	versionName, err := models.GetVersionName(c.Context(), r.DB, version)

	if err != nil {
		return err
	}

	scenarios, err := models.GetScenarios(c.Context(), r.DB, version)

	if err != nil {
		return err
	}

	if len(scenarios) == 0 {
		gitlabProjectID, _ := models.GetGitlabProjectID(c.Context(), r.DB, projectID)
		scenarios, err = gitlab.GetListScenarioFiles(gitlabProjectID, versionName)

		models.SetScenarios(c.Context(), r.DB, version, scenarios)
	}

	projectName, err := models.GetProjectName(c.Context(), r.DB, projectID)

	if err != nil {
		return err
	}

	frontend, err := models.GetFrontend(c.Context(), r.DB, projectID)

	if err != nil {
		return err
	}

	// TODO: move this to models
	frontendActiveImage, err := r.DB.Redis.HGet(c.Context(), key.Version+version+":images", frontend).Result()

	if err != nil {
		return err
	}

	// TODO: move this to models
	frontendContainerID, err := r.DB.Redis.HGet(c.Context(), key.Version+version+":containers", frontend).Result()

	if err != nil {
		return err
	}

	// TODO: move this to models
	frontendContainerState, err := r.DB.Redis.Get(c.Context(), key.Container+frontendContainerID).Result()

	if err != nil {
		return err
	}

	runningContainers = append(runningContainers, Container{
		ID:        frontendContainerID,
		ImageName: frontend + ":" + frontendActiveImage,
		State:     frontendContainerState,
	})

	backends, err := models.GetBackends(c.Context(), r.DB, projectID)

	if err != nil {
		return err
	}

	for _, backend := range backends {
		// TODO: move this to models
		backendContainerID, _ := r.DB.Redis.HGet(c.Context(), key.Version+version+":containers", backend).Result()
		backendContainerState, _ := r.DB.Redis.Get(c.Context(), key.Container+backendContainerID).Result()
		backendActiveImage, _ := r.DB.Redis.HGet(c.Context(), key.Version+version+":images", backend).Result()

		runningContainers = append(runningContainers, Container{
			ID:        backendContainerID,
			ImageName: backend + ":" + backendActiveImage,
			State:     backendContainerState,
		})
	}

	// TODO: move this to models
	environmentStatus, err := r.DB.Redis.Get(c.Context(), key.Version+version+":status").Result()

	if err != nil {
		return err
	}

	return c.Render(template.VersionTemplate, fiber.Map{
		"ProjectID":         projectID,
		"ProjectName":       projectName,
		"Scenarios":         scenarios,
		"Version":           version,
		"Containers":        runningContainers,
		"VersionName":       versionName,
		"Subdomain":         helpers.GetSubdomain(),
		"EnvironmentStatus": environmentStatus,
	}, template.MainLayout)
}

// ListProjects is
func (r *Router) ListProjects(c *fiber.Ctx) error {
	var projects []IDAndName

	projectIDs, err := models.GetAllProjects(c.Context(), r.DB)

	if err != nil {
		return err
	}

	for _, projectID := range projectIDs {
		project, _ := models.GetProjectDetail(c.Context(), r.DB, projectID)

		projects = append(projects, IDAndName{
			ID:   projectID,
			Name: project["name"],
		})
	}

	return c.Render(template.IndexTemplate, fiber.Map{
		"Projects": projects,
	}, template.MainLayout)
}

// ProjectDetail is
func (r *Router) ProjectDetail(c *fiber.Ctx) error {
	var versions []IDAndName

	projectID := c.Params("ProjectID")
	project, err := models.GetProjectDetail(c.Context(), r.DB, projectID)

	if err != nil {
		return err
	}

	backends, err := models.GetBackends(c.Context(), r.DB, projectID)

	if err != nil {
		return err
	}

	versionIDs, err := models.GetVersions(c.Context(), r.DB, projectID)

	if err != nil {
		return err
	}

	for _, versionID := range versionIDs {
		versionName, _ := models.GetVersionName(c.Context(), r.DB, versionID)

		versions = append(versions, IDAndName{
			ID:   versionID,
			Name: versionName,
		})
	}

	frontend, err := models.GetFrontend(c.Context(), r.DB, projectID)

	if err != nil {
		return err
	}

	return c.Render(template.ProjectTemplate, fiber.Map{
		"ProjectID":          projectID,
		"ProjectName":        project["name"],
		"ProjectDescription": project["description"],
		"GitlabProjectID":    project["gitlab_project_id"],
		"E2ETestRepo":        project["e2e-test-repo"],
		"Backends":           backends,
		"Frontend":           frontend,
		"Versions":           versions,
	}, template.MainLayout)
}

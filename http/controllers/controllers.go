package controllers

import "gitlab.com/fariz_delman/davinci/db"

// Router is
type Router struct {
	DB *db.Database
}

// New is
func New(db *db.Database) *Router {
	return &Router{
		DB: db,
	}
}

package services

import (
	"encoding/json"
	"net/http"

	"gitlab.com/fariz_delman/davinci/utils/helpers"
	"gitlab.com/fariz_delman/davinci/utils/key"
)

// File is
type File []struct {
	Name     string `json:"name"`
	FileType string `json:"type"`
}

const baseAPI = "https://gitlab.com/api/"
const apiVersion = "v4"
const projectsAPI = "/projects/"

const apiURL = baseAPI + apiVersion

// GetListScenarioFiles is
func GetListScenarioFiles(projectID string, version string) ([]string, error) {
	var fileResp File

	files := make([]string, 0)
	resp, err := http.Get(apiURL + projectsAPI + projectID + "/repository/tree/?ref=" + version + "&path=cypress/integration&recursive=true&private_token=" + helpers.GetGitlabPAT())

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&fileResp); err != nil {
		return nil, err
	}

	// make sure we exclude anything else except file
	for _, file := range fileResp {
		if file.FileType == key.Blob {
			files = append(files, file.Name)
		}
	}

	return files, nil
}

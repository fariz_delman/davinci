FROM golang:alpine AS builder

WORKDIR /app

RUN apk add git gcc musl-dev --no-cache

COPY . ./

RUN GOOS=linux GOARCH=amd64 go build -ldflags "-s" -o ./dist/davinci .

FROM alpine:latest

RUN apk add --no-cache tini

EXPOSE 1337

HEALTHCHECK --retries=10 CMD ["wget", "-qO-", "http://localhost:1337/__healthcheck"]

WORKDIR /app

COPY --from=builder /app/dist/davinci .
COPY --from=builder /app/web web

ENTRYPOINT ["/sbin/tini", "--"]

CMD ["./davinci"]

NAMESPACE=$(openssl rand -hex 6)

docker network create $NAMESPACE

docker run -d --network $NAMESPACE registry.gitlab.com/praedictio/tesseract-ui/ui-test:pt-202-move-to-project-level
docker run -d --network $NAMESPACE --network-alias redis redis:alpine
docker run -d --network $NAMESPACE \
  --network-alias box-db \
  -e POSTGRES_USER=redacted \
  -e POSTGRES_PASSWORD=redacted \
  -e POSTGRES_HOST=box-db \
  -e POSTGRES_DB=box-db \
  postgres:11-alpine

docker run -d --network $NAMESPACE \
  -e BOX_URL=auth-service:5000 \
  -e BOX_API_KEY=qwerty \
  -e POSTGRES_USER=redacted \
  -e POSTGRES_PASSWORD=redacted \
  -e POSTGRES_HOST=box-db \
  -e POSTGRES_DB=box-db \
  -e REDIS_PORT=6379 \
  -e JWT_SECRET=redacted \
  registry.gitlab.com/praedictio/analytic-station/station_test:b3-move-elements-to-project-level	

docker run -d --network $NAMESPACE \
  -e BOX_URL=auth-service:5000 \
  -e BOX_API_KEY=qwerty \
  -e POSTGRES_USER=box \
  -e POSTGRES_PASSWORD=password \
  -e POSTGRES_HOST=box-db \
  -e POSTGRES_DB=box-db \
  -e REDIS_PORT=6379 \
  -e JWT_SECRET=redacted \
  registry.gitlab.com/praedictio/analytic-station/station_test:b3-move-elements-to-project-level celery worker \
        --app=app.worker.celery \
        --pool=gevent \
        --concurrency=20 \
        --loglevel=INFO

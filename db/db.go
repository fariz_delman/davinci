package db

import (
	"context"
	"os"

	"github.com/go-redis/redis/v8"
)

// Database is
type Database struct {
	Redis *redis.Client
}

// Ctx is
var Ctx = context.Background()

// New is
func New() (*Database, error) {
	opt, err := redis.ParseURL(os.Getenv("REDIS_URI"))

	if err != nil {
		return nil, err
	}

	rdb := redis.NewClient(opt)

	if err := rdb.Ping(Ctx).Err(); err != nil {
		return nil, err
	}

	return &Database{
		Redis: rdb,
	}, nil
}

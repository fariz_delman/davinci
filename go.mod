module gitlab.com/fariz_delman/davinci

go 1.15

require (
	github.com/go-redis/redis/v8 v8.4.8
	github.com/gofiber/fiber/v2 v2.3.3
	github.com/gofiber/template v1.6.6
	github.com/rs/xid v1.2.1 // indirect
)

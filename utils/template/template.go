package template

// IndexTemplate is
const IndexTemplate = "index"

// VersionTemplate is
const VersionTemplate = "version"

// ProjectTemplate is
const ProjectTemplate = "project"

// MainLayout template
const MainLayout = "layouts/main"

// StaticDir is
const StaticDir = "web/static"

// VideoDir is
const VideoDir = "data/videos"

// BaseTemplateDir template
const BaseTemplateDir = "web/template"

// TemplateSuffix is
const TemplateSuffix = ".html"

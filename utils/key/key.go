package key

// Projects is
const Projects = "projects"

// Project is
const Project = "project:"

// Container is
const Container = "container:"

// Versions is
const Versions = "versions:"

// Version is
const Version = "version:"

// Frontend is
const Frontend = "frontend:"

// Backends is
const Backends = "backends:"

// Scenarios is
const Scenarios = "scenarios:"

// Blob is
const Blob = "blob"

// GitlabProjectID is
const GitlabProjectID = "gitlab_project_id"

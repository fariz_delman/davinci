package helpers

import (
	"os"
)

// IsDevelopment is
func IsDevelopment() bool {
	isProduction := os.Getenv("IS_PRODUCTION")

	return isProduction != "true"
}

// GetGitlabPAT is
func GetGitlabPAT() string {
	gitlabPAT := os.Getenv("GITLAB_PAT")

	return gitlabPAT
}

// GetSubdomain is
func GetSubdomain() string {
	subdomain := os.Getenv("SUBDOMAIN")

	return subdomain
}
